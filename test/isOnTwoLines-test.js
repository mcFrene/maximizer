﻿var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing isOnTwoLines() function", function(){
    
	var ps = 
	[
		//parallel lines
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 3n, y:-1n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 1440695n, y: 4398240n },
			{ x:-2356805n, y:-3581760n },
			{ x: -293505n, y: 5943840n },
			{ x: 2850675n, y: 3141600n },
			{ x: 1116475n, y: 4687200n },
		]),
		//intersecting lines
		new PointSet(
		1n,
		1n,
		[
			{ x:-2n, y:-2n },
			{ x: 0n, y: 2n },
			{ x: 3n, y: 5n },
			{ x: 1n, y: 4n },
			{ x: 4n, y: 2n },
			{ x: 5n, y:-1n },
			{ x:-1n, y: 0n },
			{ x: 6n, y:-4n },
		]),
		//intersecting with common point
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 2n, y:-1n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 2614385n, y: 4411680n },
			{ x:-2335625n, y:       0n },
			{ x:-1956875n, y:       0n },
			{ x: -694375n, y:       0n },
			{ x:-1285225n, y: -787800n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x:  730825n, y: 6333600n },
			{ x:-2647095n, y: 3323040n },
			{ x: 2155885n, y: 7603680n },
			{ x:-4019375n, y: 2100000n },
			{ x:-5875625n, y: 2475000n },
		]),
		//facher
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x:-1n, y:-3n },
			{ x: 5n, y: 4n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 4n, y: 3n },
		]),
		//parallel with extra point
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 1n, y:-1n },
			{ x: 3n, y:-1n },
		]),
		//intersecting with common point and extra point
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 2n, y:-1n },
			{ x: 4n, y: 3n },
		]),
		//intersecting with extra point not in the end of PS
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 4n, y: 3n },
			{ x: 2n, y:-1n },
		]),
		new PointSet(//extra point
		21n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 5199375n, y: 1100000n },
			{ x:   85625n, y: 1275000n },
			{ x: 2305625n, y:  825000n },
			{ x: 1024375n, y:-1500000n },
			{ x: 4155625n, y:  450000n },
		])
	];

    it("Should return object with rigth charactiristics for PS lying on parallel lines", function(done){
        expect(ps[0].isOnTwoLines()).to.eql({
			isOnTwoLines: true,
			indexesForLines: [ [ 0, 1, 2, 3 ], [ 4, 5, 6 ] ],
			isParallel: true,
			commonPoint: null
		});
        expect(ps[1].isOnTwoLines()).to.eql({
			isOnTwoLines: true,
			indexesForLines: [ [ 0, 2, 4, 5, 6 ], [ 1, 3 ] ],
			isParallel: true,
			commonPoint: null
		});
        done();
    });
	
	it("Should return object with rigth charactiristics for PS lying on intersecting lines", function(done){
        expect(ps[2].isOnTwoLines()).to.eql({
			isOnTwoLines: true,
			indexesForLines: [ [ 0, 1, 3, 6 ], [ 2, 4, 5, 7 ] ],
			isParallel: false,
			commonPoint: null
		});
        done();
    });

	it("Should return object with rigth charactiristics for PS lying on intersecting lines with common point", function(done){
        expect(ps[3].isOnTwoLines()).to.eql({
			isOnTwoLines: true,
			indexesForLines: [ [ 0, 4, 5, 6 ], [ 1, 2, 3, 4 ] ],
			isParallel: false,
			commonPoint: 4
		});
        expect(ps[4].isOnTwoLines()).to.eql({
			isOnTwoLines: true,
			indexesForLines: [ [ 0, 1, 3, 4, 5 ], [ 2, 5, 6 ] ],
			isParallel: false,
			commonPoint: 5
		});
        expect(ps[5].isOnTwoLines()).to.eql({
			isOnTwoLines: true,
			indexesForLines: [ [ 1, 2, 3, 4, 5 ], [ 0, 5, 6 ] ],
			isParallel: false,
			commonPoint: 5
		});
        done();
    });
	
	it("Should return object with rigth charactiristics for PS lying on facher", function(done){
        expect(ps[6].isOnTwoLines()).to.eql({
					isOnTwoLines: false,
					mostPointsOnLine: [ 0, 1, 2, 4 ],
					isParallel: null,
					commonPoint: null
				});
        expect(ps[7].isOnTwoLines()).to.eql({
					isOnTwoLines: false,
					mostPointsOnLine: [ 0, 1, 2, 3 ],
					isParallel: null,
					commonPoint: null
				});
        done();
    });
    
	it("Should return object with rigth charactiristics for PS not lying on two lines", function(done){
        expect(ps[8].isOnTwoLines()).to.eql({
					isOnTwoLines: false,
					mostPointsOnLine: [ 0, 1, 2, 3 ],
					isParallel: null,
					commonPoint: null
				});
        expect(ps[9].isOnTwoLines()).to.eql({
					isOnTwoLines: false,
					mostPointsOnLine: [ 0, 4, 5, 6 ],
					isParallel: null,
					commonPoint: null
				});
        expect(ps[10].isOnTwoLines()).to.eql({
					isOnTwoLines: false,
					mostPointsOnLine: [ 0, 4, 5, 7 ],
					isParallel: null,
					commonPoint: null
				});
        expect(ps[11].isOnTwoLines()).to.eql({
					isOnTwoLines: false,
					mostPointsOnLine: [ 0, 3, 4, 6 ],
					isParallel: null,
					commonPoint: null
				});
        done();
    });
	
});

