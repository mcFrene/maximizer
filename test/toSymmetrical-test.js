﻿var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing toSymmetrical() function", function(){
    
	var ps = 
	[
		new PointSet(//symmetrical ex1
		39n,
		5040n,
		[
			{ x: 6350400n, y:       0n },
			{ x:-6350400n, y:       0n },
			{ x: 2407860n, y:  396900n },
			{ x: 2963520n, y:       0n },
			{ x: 1189440n, y:       0n },
			{ x:  423360n, y:       0n },
			{ x: 3704400n, y: -529200n },
			{ x: 2575440n, y:  277200n },
			{ x:  926100n, y: 1455300n },
		]),
		
		new PointSet(//symmetrical img1
		35n,
		34n,
		[
			{ x: 289n, y:  0n },
			{ x:-289n, y:  0n },
			{ x:  88n, y:-39n },
			{ x:-144n, y:-15n },
			{ x:-175n, y: 48n },
			{ x:-57n, y: -24n },
			{ x: 57n, y:  24n },
		]),
		
		new PointSet(//symmetrical img2
		3n,
		2n,
		[
			{ x: 56n, y:  0n },
			{ x:-56n, y:  0n },
			{ x: 14n, y:  0n },
			{ x:-34n, y:  0n },
			{ x:-10n, y: 24n },
			{ x:-21n, y: 35n },
			{ x: 35n, y:-21n },
		]),
		
		new PointSet(//symmetrical img3
		39n,
		8n,
		[
			{ x: 240n, y:  0n },
			{ x:-240n, y:  0n },
			{ x: -91n, y:-15n },
			{ x: -35n, y:-55n },
			{ x:-140n, y: 20n },
			{ x:-112n, y:  0n },
			{ x: -16n, y:  0n },
		]),
		
		new PointSet(//symmetrical img6
		3n,
		2n,
		[
			{ x: 75n, y:  0n },
			{ x:-75n, y:  0n },
			{ x: 29n, y:-24n },
			{ x:-35n, y: 40n },
			{ x: 40n, y:-35n },
			{ x:  5n, y:  0n },
			{ x: 53n, y:  0n },
		]),
		
		new PointSet(//symmetrical img10
		15n,
		4n,
		[
			{ x:-180n, y:  0n },
			{ x: 180n, y:  0n },
			{ x: 135n, y: 45n },
			{ x: -33n, y: 21n },
			{ x:  44n, y: 32n },
			{ x:  76n, y:  0n },
			{ x: -12n, y:  0n },
		]),
		
		new PointSet(//symmetrical img13
		15n,
		8n,
		[
			{ x:-384n, y:  0n },
			{ x: 384n, y:  0n },
			{ x:-100n, y:-28n },
			{ x: 155n, y: 77n },
			{ x:-253n, y:-91n },
			{ x: -32n, y:  0n },
			{ x:  96n, y:  0n },
		]),
		
		
		new PointSet(//symmetrical img19
		210n,
		73n,
		[
			{ x: 5329n, y:   0n },
			{ x:-5329n, y:   0n },
			{ x:-3783n, y:-272n },
			{ x: 3922n, y: -42n },
			{ x: 1309n, y:-120n },
			{ x:-2376n, y:-230n },
			{ x:  237n, y:-152n },
			{ x: 3515n, y: 264n },
		]),
		
		new PointSet(//symmetrical img20
		2n,
		27n,
		[
			{ x: 2187n, y:    0n },
			{ x:-2187n, y:    0n },
			{ x: 1309n, y: 1520n },
			{ x:  899n, y: -560n },
			{ x:  623n, y: -680n },
			{ x: -665n, y:-1240n },
			{ x: 1428n, y: -330n },
			{ x:   94n, y: -910n },
		]),
		new PointSet(//not symmetrical
		1n,
		1n,
		[
			{ x: 2n, y:-1n },
			{ x:-1n, y:-2n },
			{ x: 1n, y: 2n },
			{ x: 5n, y:-4n },
			{ x: 4n, y:-2n },
			{ x: 2n, y: 4n },
			{ x: 6n, y:-3n },
			{ x:-2n, y:-4n },
		]),
		
	];

	it("Should return PS symmetrical about Y-axis if PS is symmetrical", function(done){
		expect(ps[0].toSymmetrical()).to.eql({
			char: 39n,
			base: 21337344000n,
			p:
			[
				{ x: -8961684480000n, y:              0n },
				{ x: 24644632320000n, y: -6721263360000n },
				{ x: -6721263360000n, y: -3136589568000n },
				{ x:              0n, y: -1792336896000n },
				{ x:  4694215680000n, y: -2731180032000n },
				{ x:  6721263360000n, y: -3136589568000n },
				{ x:  8961684480000n, y:              0n },
				{ x: -4694215680000n, y: -2731180032000n },
				{ x:-24644632320000n, y: -6721263360000n },
			]}
		);
		expect(ps[1].toSymmetrical()).to.eql({
			char: 35n,
			base: 18496n,
			p:
			[
				{ x:-147968n, y:     0n },
				{ x: 120224n, y: 27744n },
				{ x:-120224n, y: 27744n },
				{ x:  27744n, y: 27744n },
				{ x: 147968n, y:     0n },
				{ x: -27744n, y: 27744n },
				{ x:      0n, y:     0n },
			]}
		);
		expect(ps[2].toSymmetrical()).to.eql({
			char: 3n,
			base: 84n,
			p:
			[
				{ x: -882n, y:    0n },
				{ x: 1470n, y:-2352n },
				{ x:    0n, y: -882n },
				{ x: 1008n, y:-1890n },
				{ x:-1008n, y:-1890n },
				{ x:-1470n, y:-2352n },
				{ x:  882n, y:    0n },
			]}
		);
		expect(ps[3].toSymmetrical()).to.eql({
			char: 39n,
			base: 3520n,
			p:
			[
				{ x:-96800n, y:     0n },
				{ x: 35200n, y:-26400n },
				{ x: 26400n, y:-14080n },
				{ x: 96800n, y:     0n },
				{ x:-35200n, y:-26400n },
				{ x:     0n, y:-19360n },
				{ x:-26400n, y:-14080n },
			]}
		);
		expect(ps[4].toSymmetrical()).to.eql({
			char: 3n,
			base: 140n,
			p:
			[
				{ x:-2450n, y:    0n },
				{ x: 2800n, y:-5250n },
				{ x: 1680n, y: -770n },
				{ x:-2800n, y:-5250n },
				{ x: 2450n, y:    0n },
				{ x:    0n, y:-2450n },
				{ x:-1680n, y: -770n },
			]}
		);
		expect(ps[5].toSymmetrical()).to.eql({
			char: 15n,
			base: 720n,
			p:
			[
				{ x:     0n, y: 16200n },
				{ x:-16200n, y:     0n },
				{ x: 16200n, y:     0n },
				{ x:  7560n, y:  8640n },
				{ x: 11520n, y:  4680n },
				{ x:-11520n, y:  4680n },
				{ x: -7560n, y:  8640n },
			]}
		);
		expect(ps[6].toSymmetrical()).to.eql({
			char: 15n,
			base: 4928n,
			p:
			[
				{ x:-189728n, y:     0n },
				{ x: 224224n, y:-59136n },
				{ x: -68992n, y:-36960n },
				{ x: 189728n, y:     0n },
				{ x:-224224n, y:-59136n },
				{ x:      0n, y:-27104n },
				{ x:  68992n, y:-36960n },
			]}
		);
		expect(ps[7].toSymmetrical()).to.eql({
			char: 210n,
			base: 724744n,
			p:
			[
				{ x:-49282592n, y:       0n },
				{ x: 47833104n, y:-2898976n },
				{ x: 49282592n, y:       0n },
				{ x:-34062968n, y:       0n },
				{ x: -5797952n, y:       0n },
				{ x: 34062968n, y:       0n },
				{ x:  5797952n, y:       0n },
				{ x:-47833104n, y:-2898976n },
			]}
		);
		expect(ps[8].toSymmetrical()).to.eql({
			char: 2n,
			base: 90396n,
			p:
			[
				{ x:-5604552n, y:       0n },
				{ x: 6870096n, y:-5423760n },
				{ x:-6870096n, y:-5423760n },
				{ x: -542376n, y:       0n },
				{ x:  542376n, y:       0n },
				{ x: 5604552n, y:       0n },
				{ x:-2621484n, y:       0n },
				{ x: 2621484n, y:       0n },
			]}
		);
		done();
	});
	
	

	it("Should return null if PS is not symmetrical", function(done){
		expect(ps[9].toSymmetrical()).to.eql(null);
		done();
	});
	
});

