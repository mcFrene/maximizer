var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing isIPS() function", function(){
    
	var ps = 
	[
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y:      0n},
			{ x:0n, y:      3n},
			{ x:4n, y:      0n},
		]),
		new PointSet(
		3n,
		2n,
		[
			{ x: 56n, y:       0n},
			{ x:-56n, y:       0n},
			{ x: 14n, y:       0n},
			{ x:-34n, y:       0n},
			{ x:-10n, y:      24n},
			{ x:-21n, y:      35n},
			{ x: 35n, y:     -21n},
		]),
		new PointSet(
		39n,
		8n,
		[
			{ x: 5040n, y:       0n},
			{ x:-5040n, y:       0n},
			{ x: 1911n, y:     315n},
			{ x: 2352n, y:       0n},
			{ x:  944n, y:       0n},
			{ x:  336n, y:       0n},
			{ x: 2940n, y:    -420n},
			{ x: 2044n, y:     220n},
			{ x:  735n, y:    1155n},
		]),
		new PointSet(
		255255n,
		1n,
		[
			{x:        0n, y:    0n},
			{x:  8171520n, y:    0n},
			{x:  3482640n, y:-5040n},
			{x:  5202960n, y:-5040n},
			{x:  1384320n, y:    0n},
			{x:  6451200n, y:    0n},
			{x:  6289920n, y:    0n},
			{x:   514080n, y:    0n},
			{x:  2234400n, y:    0n},
			{x:  2395680n, y:    0n},
			{x:  7301280n, y:    0n},
			{x:  5150880n, y:    0n},
			{x:  3534720n, y:    0n},
			{x:  4384800n, y:    0n},
			{x:  4300800n, y:    0n},
			{x: 24612000n, y:    0n},
			{x:  8685600n, y:    0n},
			{x:  7666176n, y:    0n},
			{x:  5951008n, y:    0n},
			{x:  4656672n, y:    0n},
			{x:  4028928n, y:    0n},
			{x:  2734592n, y:    0n},
			{x:  1019424n, y:    0n},
			{x:-15926400n, y:    0n},
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 0n },
			{ x: 2n, y: 1n },
			{ x: 2n, y:-1n },
			{ x: 4n, y: 0n },
			{ x:-1n, y:-3n },
		]),
	];

    it("Should return true for IPS", function(done){
        expect(ps[0].isIPS()).to.eql(true);
        expect(ps[1].isIPS()).to.eql(true);
        expect(ps[2].isIPS()).to.eql(true);
        expect(ps[3].isIPS()).to.eql(true);
        done();
    });
	
	it("Should return false for other PS", function(done){
        expect(ps[4].isIPS()).to.eql(false);
        expect(ps[5].isIPS()).to.eql(false);
        done();
    });
    
});
