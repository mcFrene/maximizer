﻿var mocha  = require ( 'mocha' )
  , assert = require ( 'chai' ).assert
  , expect = require ( 'chai' ).expect
  , PointSet = require ( '../PointSet.js' )
  ;

describe( "Testing rotateParallel() function", function(){

	var ps =
	[
		new PointSet(//oblique parallel
    5n,
    1n,
    [
        { x:-5n, y:-1n },
        { x:-3n, y: 0n },
        { x:-1n, y: 1n },
        { x: 3n, y: 3n },
        { x: 5n, y: 4n },
        { x: 1n, y:-1n },
        { x: 3n, y: 0n },
    ]
	),
	new PointSet(//most points on X
    1n,
    1n,
    [
        { x:-5n, y: 0n },
        { x:-3n, y: 0n },
        { x:-1n, y: 0n },
        { x: 3n, y: 0n },
        { x: 5n, y: 0n },
        { x: 5n, y:-3n },
        { x: 7n, y:-3n },
    ]
	),
	new PointSet(//most points on line parallel to X
    1n,
    1n,
    [
        { x:-5n, y: 1n },
        { x:-3n, y: 1n },
        { x:-1n, y: 1n },
        { x: 3n, y: 1n },
        { x: 5n, y: 1n },
        { x: 5n, y:-3n },
        { x: 7n, y:-3n },
    ]
	),
	new PointSet(//rotated
    1n,
    1n,
    [
        { x: 0n, y: 0n },
        { x:-3n, y: 0n },
        { x:-5n, y: 0n },
        { x: 3n, y: 0n },
        { x: 5n, y: 0n },
        { x: 5n, y:-3n },
        { x: 7n, y:-3n },
    ]
	),
	new PointSet(//most points on Y
    1n,
    1n,
    [
        { x: 0n, y: -2n },
        { x: 0n, y: -4n },
        { x: 0n, y:  5n },
        { x: 0n, y:  8n },
        { x: 0n, y: 10n },
        { x: 3n, y:  3n },
        { x: 3n, y:  7n },
    ]
	),
	new PointSet(//most points on line parallel to Y
    1n,
    1n,
    [
        { x:-1n, y: -2n },
        { x:-1n, y: -4n },
        { x:-1n, y:  5n },
        { x:-1n, y:  8n },
        { x:-1n, y: 10n },
        { x: 3n, y:  3n },
        { x: 3n, y:  7n },
    ]
	),
	new PointSet(//not parallel
    1n,
    1n,
    [
        { x:  -1n, y:    -2n },
        { x:  -1n, y:    -4n },
        { x:  -1n, y:     5n },
        { x:  -1n, y:     8n },
        { x:  -1n, y:    10n },
        { x: 123n, y: 34124n },
        { x:   3n, y:     3n },
        { x:   3n, y:     7n },
    ]
	),
	];

	it( "Should return correct rotated PS", function (done){
        expect(
			(new PointSet(
				1n,
				1n,
				[
					{ x:  0n, y:  -3n },
					{ x:  0n, y:   3n },
					{ x:  4n, y:   0n },
					{ x: -4n, y:   0n },
					{ x:  8n, y:   3n },
				]
			)).rotateParallel()
		).to.eql(
			{
				char: 1n,
				base: 5n,
				p: [
					{ x:  0n, y:   0n },
					{ x: 18n, y:  24n },
					{ x: 25n, y:   0n },
					{ x: -7n, y:  24n },
					{ x: 50n, y:   0n },
				]
			}
		);


        expect ( ps[1].rotateParallel() ).to.eql( {
		char: 1n,
		base: 2n,
		p:
		[
			{ x:  0n, y: 0n },
			{ x:  4n, y: 0n },
			{ x:  8n, y: 0n },
			{ x: 16n, y: 0n },
			{ x: 20n, y: 0n },
			{ x: 20n, y:-6n },
			{ x: 24n, y:-6n },
		] } );
        expect ( ps[2].rotateParallel() ).to.eql( {
		char: 1n,
		base: 2n,
		p:
		[
			{ x:  0n, y: 0n },
			{ x:  4n, y: 0n },
			{ x:  8n, y: 0n },
			{ x: 16n, y: 0n },
			{ x: 20n, y: 0n },
			{ x: 20n, y:-8n },
			{ x: 24n, y:-8n },
		] } );
        expect ( ps[3].rotateParallel() ).to.eql( {
		char: 1n,
		base: 3n,
		p:
		[
			{ x:  0n, y: 0n },
			{ x:  9n, y: 0n },
			{ x: 15n, y: 0n },
			{ x: -9n, y: 0n },
			{ x:-15n, y: 0n },
			{ x:-15n, y: 9n },
			{ x:-21n, y: 9n },
		] } );
        expect ( ps[4].rotateParallel() ).to.eql( {
		char: 1n,
		base: 2n,
		p:
		[
			{ x:  0n, y: 0n },
			{ x:  4n, y: 0n },
			{ x:-14n, y: 0n },
			{ x:-20n, y: 0n },
			{ x:-24n, y: 0n },
			{ x:-10n, y: 6n },
			{ x:-18n, y: 6n },
		] } );
        expect ( ps[5].rotateParallel() ).to.eql( {
		char: 1n,
		base: 2n,
		p:
		[
			{ x:  0n, y: 0n },
			{ x:  4n, y: 0n },
			{ x:-14n, y: 0n },
			{ x:-20n, y: 0n },
			{ x:-24n, y: 0n },
			{ x:-10n, y: 8n },
			{ x:-18n, y: 8n },
		] } );
        done();
    } );

	it( "Should return null for PS which are not lying on parallel lines", function (done){
        expect ( ps[6].rotateParallel() ).to.eql( null );
        done();
    } );

});

