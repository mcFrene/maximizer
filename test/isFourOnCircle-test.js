﻿var mocha  = require ( 'mocha' )
  , assert = require ( 'chai' ).assert
  , expect = require ( 'chai' ).expect
  , PointSet = require ( '../PointSet.js' )
  ;

describe( "Testing isFourOnCircle() function", function(){
    
	var ps = 
	[
		new PointSet(//on circle with center
		1n,
		1n,
		[
			{ x: 0n, y: 0n },
			{ x: 0n, y: 5n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 4n },
			{ x: 3n, y:-4n }
		]),
		new PointSet(//on circle
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 4n },
			{ x: 3n, y:-4n }
		]),
		new PointSet(//not on circle
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x: 1n, y: 0n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 4n }
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-5n, y: 0n },
			{ x: 2n, y:-3n },
			{ x:-2n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 7n, y:-3n },
			{ x:10n, y:-6n }
		]),
		new PointSet(
		2002n,
		2227n,
		[
			{ x: 0n,        y: 0n       },
			{ x: 49595290n, y: 0n       },
			{ x: 26127018n, y: 932064n  },
			{ x: 32142553n, y: 411864n  },
			{ x: 17615968n, y: 238464n  },
			{ x: 7344908n,  y: 411864n  },
			{ x: 19079044n, y:-54168n   },
		])
	];

	it( "Should return true is there are four points on circle in PS", function (done){
        expect ( ps[0].isFourOnCircle() ).to.eql( true );
        expect ( ps[1].isFourOnCircle() ).to.eql( true );
        done();
    } );
	
	it( "Should return false if every four points are not circle", function (done){
        expect ( ps[2].isFourOnCircle() ).to.eql ( false );
        expect ( ps[3].isFourOnCircle() ).to.eql ( false );
        expect ( ps[4].isFourOnCircle() ).to.eql ( false );
        done();
    });
	
});

