const PointSet = require('./PointSet');
const removeFromArray = require('./removeFromArray');

module.exports = class PointSetDraft extends PointSet{
    constructor(char, base, points, clique){
        super(char, base, points);
        this.clique = clique || [];
    }
    countFriends(p){
        var friends = 0;
        for(let j = 0; j < this.p.length; j++){
            if(this.isDistanceIntegral(p, this.p[j])){
                friends++;
            }
        }
        return friends;
    }
    getFriendsOf(i){
        var friends = [];
        for(let j = 0; j < this.p.length; j++){
            if(i !== j && this.isDistanceIntegral(this.p[i], this.p[j])){
                friends.push(this.p[j]);
            }
        }
        return friends;
    }
    filter(friendsNeeded){
        for(let i = 0; i < this.p.length; i++){
            if(this.countFriends(this.p[i]) < friendsNeeded){
                removeFromArray(this.p, i);
                // TODO: i--; and filterWhilePossible(...){...}
                i = 0;
            }
        }
    }
    enlargeClique(){
        for(let i = this.p.length - 1; i >= 0; i--){
            if(this.countFriends(this.p[i]) === this.p.length){
                this.clique.push(this.p[i]);
                removeFromArray(this.p, i);
            }
        }
    }
    isFacherNaive(){
        var n = 2;
        for(let arr of [this.clique, this.p]){
            for(let i = 0; i < arr.length && n; i++){
                if(arr[i].y){
                    n--;
                }
            }
        }
        //console.log(n);
        return !!n;
    }
    fork(){
		//console.log('*********************');
		var length_before = this.p.length;
		//console.log(length_before);
        var friends = this.getFriendsOf(this.p.length - 1);
        var p = this.p.pop();
        var child = new PointSetDraft(
            this.char,
            this.base,
            friends,
            this.clique.slice().concat(p)
        );

		if(length_before <= this.p.length){
			console.log('length_before <= this.p.length');
			console.log(this);
			console.log(child);
		}
		if(length_before <= child.p.length){
			console.log('length_before <= child.p.length');
			console.log(this);
			console.log(child);
		}
		//console.log(this.p.length);
		//console.log(child.p.length);

        return child;
    }
};
