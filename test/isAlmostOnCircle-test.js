﻿var mocha  = require ( 'mocha' )
  , assert = require ( 'chai' ).assert
  , expect = require ( 'chai' ).expect
  , PointSet = require ( '../PointSet.js' )
  ;

describe( "Testing isAlmostOnCircle() function", function(){
    
	var ps = 
	[
		new PointSet(//on circle with center
		1n,
		1n,
		[
			{ x: 0n, y: 0n },
			{ x: 0n, y: 5n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 4n },
			{ x: 3n, y:-4n }
		]),
		new PointSet(//almost on circle
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x: 1n, y: 0n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 4n },
			{ x: 3n, y:-4n }
		]),
		new PointSet(//not almost on circle
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x: 1n, y: 0n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 7n },
			{ x: 3n, y: 4n },
			{ x: 3n, y:-4n }
		]),
		new PointSet(//circular
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-5n, y: 0n },
			{ x: 5n, y: 0n },
			{ x: 4n, y:-3n },
			{ x: 3n, y: 4n },
			{ x: 3n, y:-4n }
		])
	];

    it ( "Should return object with center for PS lying on cirle with center", function (done){
        expect ( ps[0].isAlmostOnCircle() ).to.eql({
			isOnCircleWithCenter: true,
			initialPS: new PointSet(
				1n,
				1n,
				[
					{ x: 0n, y: 0n },
					{ x: 0n, y: 5n },
					{ x: 5n, y: 0n },
					{ x: 3n, y: 4n },
					{ x: 3n, y:-4n }
				]),
			indexOfCenter: 0,
			Center: { x: 0n, y: 0n },
			PointsOnCircle: [ 1, 2, 3, 4 ]
		});
        done();
    });
	
	it( "Should return object with extra point for PS which are almost on circle", function (done){
        expect ( ps[1].isAlmostOnCircle() ).to.eql({
			isAlmostCircle: true,
			initialPS: new PointSet(
				1n,
				1n,
				[
					{ x: 0n, y: 5n },
					{ x: 1n, y: 0n },
					{ x: 5n, y: 0n },
					{ x: 3n, y: 4n },
					{ x: 3n, y:-4n }
				]),
			notOnCircleIndex: 1,
			notOnCirclePoint: { x: 1n, y: 0n },
			PointsOnCircle: [ 0, 2, 3, 4 ]
		});
        done();
    } );
	
	it( "Should return null for PS which are not almost on circle or circular", function (done){
        expect ( ps[2].isAlmostOnCircle() ).to.eql ( null );
        expect ( ps[3].isAlmostOnCircle() ).to.eql ( null );
        done();
    });
	
});

